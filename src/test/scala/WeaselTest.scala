import chisel3.iotesters.{Driver, PeekPokeTester, TesterOptionsManager}

class WeaselTest(c: Weasel, n: Int) extends PeekPokeTester(c){
  val seedSentence =     "WDLTMNLT DTJBKWIRZREZLMQCO P"
  val expectedSentence = "METHINKS IT IS LIKE A WEASEL"
  val length = seedSentence.length
  poke(c.io.sentenceLength, length)

  for (i <- 0 until length) {
    poke(c.io.seedSentence(i), seedSentence(i))
    poke(c.io.targetSentence(i), expectedSentence(i))
  }
  step(10)
  poke(c.io.start, true)
//  printf("Max Score %d", peek(c.io.maxScore))
  while (peek(c.io.completed) == 0) {
    for (j <- 0 until length) {
      printf("%d ", peek(c.io.out(j)))
    }
    printf("\n")
    for (j <- 0 until length) {
      printf("%d ", expectedSentence(j).toInt)
    }
    printf(" Current Score %d \n\n", peek(c.io.maxScore))
    step(1)
  }
}

object WeaselTest {
  def main(args: Array[String]): Unit = {
    val num = 10
    Driver.execute(() => new Weasel(num, 28), new TesterOptionsManager()) {
      c => new WeaselTest(c, num)
    }
  }
}
