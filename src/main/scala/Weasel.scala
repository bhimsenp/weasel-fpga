import chisel3._
import chisel3.util.log2Ceil
import com.thoughtworks.ga.{GABundle, GeneticAlgorithm, GeneticAlgorithmConfiguration, NextGenerationType}

class Weasel(numOfMutators: Int, maxLetters: Int) extends Module{

  val io = IO(new Bundle{
    val seedSentence = Input(Vec(maxLetters, UInt(8.W)))
    val targetSentence = Input(Vec(maxLetters, UInt(8.W)))
    val sentenceLength = Input(UInt(log2Ceil(maxLetters).W))
    val out = Output(Vec(maxLetters, UInt(8.W)))
    val maxScore = Output(UInt(log2Ceil(maxLetters).W))
    val completed = Output(Bool())
    val start = Input(Bool())
  })

  val fitnessFunction = new WeaselFitnessFunction()
  val mutationFunction = new WeaselMutationFunction()
  val haltingConditionFunction = new WeaselHaltingCondition()

  val bundle = new WeaselBundle(maxLetters)

  val config = new GeneticAlgorithmConfiguration(numOfMutators, maxLetters, NextGenerationType.Single, fitnessFunction, mutationFunction, haltingConditionFunction, bundle)

  val geneticAlgorithm = Module(new GeneticAlgorithm(config))

  geneticAlgorithm.io.seedSequence := io.seedSentence
  geneticAlgorithm.io.sequenceLength := io.sentenceLength
  geneticAlgorithm.io.start := io.start
  geneticAlgorithm.io.targetSentence := io.targetSentence
  io.out := geneticAlgorithm.io.out
  io.maxScore := geneticAlgorithm.io.maxScore
  io.completed := geneticAlgorithm.io.completed

}

class WeaselBundle(maxLetters: Int) extends GABundle(maxLetters) {
  val targetSentence = Input(Vec(maxLetters, UInt(8.W)))
}

object Weasel extends App {
  chisel3.Driver.execute(args, () => new Weasel(3, 5))
}
