import chisel3.util.MuxLookup
import chisel3.{Mux, UInt, Vec, _}
import com.thoughtworks.ga.{LFSR, MutationFunction}

class WeaselMutationFunction() extends MutationFunction{
  override def mutate(chromosomeSequence: Vec[UInt], chromosomeLength: UInt): Vec[UInt] = {
    val alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "
    val lookup = Array.range(0, alphabets.length).map(i => i.U -> alphabets(i).U(8.W))
    val random = Math.round(Math.random() * 100).toInt
    val shouldMutateAtAll = LFSR(10, random)
    val mutated = VecInit(chromosomeSequence.zipWithIndex.map { case (letter, i) =>
      val shouldMutate = i.U < chromosomeLength
      val lfsrForMutation = LFSR(10, i + 1) + random.U
      val lfsrForAlphabet = LFSR(10, i + 10) + random.U

      val mutatedLetter = Mux(lfsrForMutation % 100.U < 5.U && shouldMutate, MuxLookup(lfsrForAlphabet % alphabets.length.U, ' '.U(8.W), lookup), letter)
      mutatedLetter
    })
    mutated
  }
}
