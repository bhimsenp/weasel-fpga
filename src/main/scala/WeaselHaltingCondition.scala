import chisel3._
import com.thoughtworks.ga.HaltingConditionFunction

class WeaselHaltingCondition() extends HaltingConditionFunction[WeaselBundle]{
  override def shouldHalt(bundle: WeaselBundle, maxScore: UInt): Bool = maxScore === bundle.sequenceLength
}
