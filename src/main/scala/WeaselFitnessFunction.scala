import chisel3._
import chisel3.util.Cat
import com.thoughtworks.ga.FitnessFunction

class WeaselFitnessFunction extends FitnessFunction[WeaselBundle]{
  override def calculate(bundle: WeaselBundle, chromosomeSequence: Vec[UInt]): UInt = {
    val targetSequence = bundle.targetSentence
    chromosomeSequence.zipWithIndex
      .map{case (letter, i) => Cat(0.U(7.W), letter === targetSequence(i))}
      .reduce((s1, s2) => s1 + s2)
  }

  override def fitnessToScore(maxScore: UInt): UInt = maxScore
}
