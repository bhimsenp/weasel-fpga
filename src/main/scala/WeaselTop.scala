import chisel3._
import chisel3.core.{withClock, withClockAndReset}
import chisel3.util.{Cat, MuxCase, MuxLookup, log2Ceil}

class WeaselTop(numOfMutators: Int, maxLetters: Int) extends Module{
  val io = IO(new Bundle{
    val address = Output(UInt(16.W))
    val writeData = Output(UInt(8.W))
    val writeEnable = Output(Bool())
    val readData = Input(UInt(8.W))
    val ledOut = Output(UInt(4.W))
  })

  val dataOutOffset = 110
  val startSignalAddress = 108

  val seedSentence = Reg(Vec(maxLetters, UInt(8.W)))
  val targetSentence = Reg(Vec(maxLetters, UInt(8.W)))
  val outputSentence = Reg(Vec(maxLetters, UInt(8.W)))
  val sentenceLength = Reg(UInt(log2Ceil(maxLetters).W))
  val randomSeed = Reg(Vec(numOfMutators, UInt(7.W)))
  val inputReady = RegInit(Bool(), false.B)
  val addressCounter = RegInit(UInt(8.W), 0.U)
  val shouldStartWeasel = RegInit(Bool(), false.B)

  val sentenceCounter = RegInit(UInt(log2Ceil(maxLetters).W), 0.U)
  val weasel = Module(new Weasel(numOfMutators, maxLetters))
  weasel.io.seedSentence := seedSentence
  weasel.io.targetSentence := targetSentence
  weasel.io.sentenceLength := sentenceLength
  weasel.io.start := shouldStartWeasel
  outputSentence := weasel.io.out

  io.address := addressCounter

  val seedSentenceMapping = Array.range(0, maxLetters).map(i => i.U -> seedSentence(i))
  inputReady := Mux(addressCounter === 0.U, io.readData, inputReady)
  shouldStartWeasel := Mux(addressCounter === startSignalAddress.U, io.readData, shouldStartWeasel)
  sentenceLength := Mux(addressCounter === 2.U, io.readData, sentenceLength)
  seedSentence := Array.range(0, maxLetters).map(i => Mux(addressCounter === (i+3).U, io.readData, seedSentence(i)))
  targetSentence := Array.range(0, maxLetters).map(i => Mux(addressCounter === (i+31).U, io.readData, targetSentence(i)))
  randomSeed := Array.range(0, numOfMutators).map(i => Mux(addressCounter === (i+59).U, io.readData, randomSeed(i)))

  io.writeEnable := addressCounter > dataOutOffset.U

  val outputSentenceMapping = Array.range(0, maxLetters).map(i => (i+dataOutOffset).U -> outputSentence(i))
  val writeMapping = MuxCase(MuxLookup(addressCounter, 1.U, outputSentenceMapping),
    Array((addressCounter === (dataOutOffset + 28).U) -> weasel.io.completed,
    (addressCounter === (dataOutOffset + 29).U) -> weasel.io.maxScore)
  )
  io.writeData := RegNext(writeMapping)
  
  val isReading = addressCounter < dataOutOffset.U && inputReady
  io.ledOut := Cat(5.U, shouldStartWeasel)
  when(inputReady) {
    val addressIncrement = addressCounter + 1.U
    addressCounter := Mux(addressCounter === (dataOutOffset + 30).U, dataOutOffset.U, addressIncrement)
  }
}

object WeaselTop extends App {
  chisel3.Driver.execute(args, () => new WeaselTop(15, 28))
}
