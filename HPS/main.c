/*
This program demonstrate how to use hps communicate with FPGA through light AXI Bridge.
uses should program the FPGA by GHRD project before executing the program
refer to user manual chapter 7 for details about the demo
*/


#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include "hps_0.h"

#define HW_REGS_BASE ( 0xff200000 )
#define HW_REGS_SPAN ( 0x00010000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

int main() {

	void *virtual_base;
	int fd;
	void *fpga_addr;

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span

	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}

	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );

	if( virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( fd );
		return( 1 );
	}

	fpga_addr=virtual_base;

	int i=0;

	char seedSentence[28] =   "WDLTMNLT DTJBKWIRZREZL";
	char targetSentence[28] = "METHINKS IT IS LIKE A ";
	*(char *)(fpga_addr + 1) = (char)22;

	printf("%d\n", *(char *)fpga_addr);

    while(i<28) {
        *(char *)(fpga_addr + i + 2) = seedSentence[i];
        *(char *)(fpga_addr + i + 30) = targetSentence[i];
        i++;
    }
    i=0;
    srand(time(0));
    int randomNumbers[15] = {6, 85, 56, 88, 60, 30, 6, 43, 48, 53, 75, 47, 7, 27, 17};
    while(i<15) {
        char random = rand() % 100;
        *(char *)(fpga_addr + i + 58) = random;
        printf("%d ", *(char *)(fpga_addr + i + 58));
        i++;
    }
    printf("\n");
    i=0;
    *(char *)(fpga_addr) = (char)1;
    *(char *)(fpga_addr) = (char)0;

    *(char *)(fpga_addr + 108) = (char)3;
//    *(char *)(fpga_addr + 108) = (char)0;
	usleep( 100*1000 );
	while( i < 50 ) {
	    int j=0;
		while(j < 28) {
		    printf("%c", *(char *)(fpga_addr + j + 111));
		    j++;
		}
        printf("_______ Current score: %d ", *(char *)(fpga_addr + 140));
		printf("\n");
		// wait 100ms
		usleep( 300*1000 );
		i++;

	}// while

    printf("\n%d\n", *(char *)(fpga_addr + 139));
	// clean up our memory mapping and exit

	if( munmap( virtual_base, HW_REGS_SPAN ) != 0 ) {
		printf( "ERROR: munmap() failed...\n" );
		close( fd );
		return( 1 );
	}

	close( fd );

	return( 0 );
}
